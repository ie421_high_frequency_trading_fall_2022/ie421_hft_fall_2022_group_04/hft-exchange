# A C++ Multithreaded HFT Exchange Stack confiming to NASDAQ ITCH/OUCH Standards

This repository holds scripts to instantiate VMs for different parts of an Exchange. In this project, we have one Gateway, one Tickerplant, and one Exchange. As of 12/14/2022, The Exchange is the only part that has been thoroughly completed and it is only missing the networking portions while the Gateway and Tickerplant are also missing the networking code to connect to the Exchange.

[**To Source Code Documentation**](documentation/SRC_DOCUMENTATION.md)

## Table of Contents

- [A C++ Multithreaded HFT Exchange Stack confiming to NASDAQ ITCH/OUCH Standards](#a-c-multithreaded-hft-exchange-stack-confiming-to-nasdaq-itchouch-standards)
  - [Table of Contents](#table-of-contents)
  - [Team](#team)
    - [**Benjamin Nguyen (Group Lead)**](#benjamin-nguyen-group-lead)
    - [**Justin Kuhn**](#justin-kuhn)
    - [**Yuyang Zhao**](#yuyang-zhao)
  - [Project Description](#project-description)
  - [Technologies](#technologies)
  - [Components](#components)
    - [Exchange](#exchange)
    - [HFT](#hft)
  - [Git Repo Layout](#git-repo-layout)
    - [Subproject layout](#subproject-layout)
  - [Instructions for using the project](#instructions-for-using-the-project)
    - [**Project Dependencies:**](#project-dependencies)
    - [1) Git checkout](#1-git-checkout)
    - [2) Create the VMs](#2-create-the-vms)
    - [3) SSH into VMs](#3-ssh-into-vms)
      - [SSH with command line](#ssh-with-command-line)
      - [SSH with VSCode](#ssh-with-vscode)
    - [4) Running the program](#4-running-the-program)
  - [Testing](#testing)
    - [Running the tests](#running-the-tests)
  - [Project Results](#project-results)
  - [Screenshots and Example Outputs](#screenshots-and-example-outputs)
  - [Postmortem Summary](#postmortem-summary)
  - [TODOs](#todos)

## Team

### **Benjamin Nguyen (Group Lead)**

Hi! I currently an Undergraduate at UIUC graduating in December 2022. I am the CTO of [Quant Illinois at UIUC](https://www.quantillinois.com/) and am very interested in the High Frequency Trading space, specifically in regards to Software Engineering, designing systems, and optimization. I want to explore more of different market microstructures and help optimize trading algorithms for specific applications. I am planning on pursuing an MCS at UIUC in Fall 2023.

<img src=https://brand.linkedin.com/content/dam/me/business/en-us/amp/brand-site/v2/bg/LI-Bug.svg.original.svg alt="isolated" width="24"/> [www.linkedin.com/in/benjaminkhacnguyen](www.linkedin.com/in/benjaminkhacnguyen)

<img src=https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png alt="isolated" width="20"/> [www.github.com/ben44496](www.github.com/ben44496)

### **Justin Kuhn**

### **Yuyang Zhao**

## Project Description

This project was done as part of IE 421 - High Frequency Trading, taught by [Professor David Lariviere](https://davidl.web.illinois.edu/) at the University of Illinois at Urbana-Champaign.

This project is divided into three parts: the Order Matching Engine (OME), the Gateway, and the Tickerplant. Around 90% of the code is located in the OME repository due to the nature of the OME being where the trades are being executed, with the Gateway performing the networking switching depending on the stock symbol of the packet. We have designed the Exchange to purposely use the NASDAQ [ITCHV5.0](https://www.nasdaqtrader.com/content/technicalsupport/specifications/dataproducts/NQTVITCHSpecification.pdf) and [OUCHv4.2](http://www.nasdaqtrader.com/content/technicalsupport/specifications/tradingproducts/ouch4.2.pdf) specifications for orders to the exchange as well as market data feed. The idea behind this is both to gain familiarity with this protocol, as well as to provide market-by-order (level 3) data feed as part of our deep dive into HFT technologies.

The Order Matching Engine (OME) is the "brain" of the Exchange. Order Entry (in NASDAQ OUCH format) is forwarded from the Gateway to the correct OME that has the traded stock symbol. These messages are then parsed, updated in the Orderbook, and then sends the Outbound OUCH and ITCH messages to the corresponding Gateway and Tickerplant respectively. The OME contains the OME code, the Orderbook book building, code for parsing structs to and from unsigned char buffer arrays from network IO, and struct information. The OME is a TCP server that maintains connections to the Gateways. The OME is also treated as a UDP server that maintains connections to the Tickerplant (not implemented yet). There is also both behaviour driven testing and unit testing available for the OME code that uses the C++ [Catch2](https://github.com/catchorg/Catch2) Testing Framework which is expanded upon in the [Technologies](#technologies) and [Testing](#testing) sections.

The Gateway and Tickerplants are very lightweight TCP and UDP servers, respectively, and they maintain a connection between both Market Participants and the Order Matching Engines. The Gateway has additional functionality to send the message to the correct OME based on the stock symbol. It will also throw away any messages that are not for any of the Market Participants on its network, and this is done through the "MPID" functionality supplied by the orders. As of 12/14/2022, the only difference between the message sent by the OME and the one send to the Market Participant from the gateway is that the gateway will strip the mpid attribute at the end of the message.

## Technologies

See [TECHNOLOGIES.md](documentation/TECHNOLOGIES.md)

## Components

### Exchange

See [EXCHANGE_COMPONENTS.md](documentation/EXCHANGE_COMPONENTS.md) // TODO: Finish

### HFT

This is not yet available. In the future, you will be able to see [HFT_COMPONENTS.md](documentation/HFT_COMPONENTS.md)

## Git Repo Layout

- [documentation/](documentation/)
  - Contains longer Markdown files with project-specific information. Documents are linked to from this README as well.
- [scripts/](scripts/)
  - A set of (minimal) provisioning scripts for the OME, Gateway, and Tickerplant. If you need to navigate the repositories for development you will need to uncomment some lines in these files as needed.
- [.gitignore](.gitignore)
  - Files to ignore for Git. Usually just the `.vagrant/` and associated development specific files.
- [.markdownlint.json](.markdownlint.json)
  - JSON config file for [Markdown All in One VSCode extension by Yu Zhang](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
- [README.md](README.md)
- [Vagrantfile](Vagrantfile)
  - Vagrant provisioning setup script

### Subproject layout

For explanation on the layout for how the individual project repositories are laid out, please go to this link: [GIT_REPO_PROJECT_LAYOUT.md](GIT_REPO_PROJECT_LAYOUT.md)

## Instructions for using the project

All the code will be instantiated within each container and will automatically git checkout the main branch of the respective code.

### **Project Dependencies:**

- Vagrant v2.3.0

> Note: To check what versions we are using for our tools please see [TECHNOLOGIES.md](documentation/TECHNOLOGIES.md).

### 1) Git checkout

1. Clone the code to your local machine.
   1. Open a terminal
   2. Navigate to your desired folder with `cd`
   3. In your terminal, run `git clone https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2022/ie421_hft_fall_2022_group_04/hft-exchange.git exchange-ome/`

### 2) Create the VMs

1. Continue in your terminal, run `cd exchange-ome/`
2. Run `vagrant up --provision`.
3. Wait for the provisioning to finish. Watch the output for errors. If there are any errors, please feel free to make an issue on our repository or try and debug it yourself.
4. Run `vagrant status` to make sure that you have 3 VMs provisioned named `ome1`, `gateway1`, and `tp1`, with all three statuses set to "running."

Note: All three VMs will then be provisioned and automatically clone the repositories at `/home/vagrant` which is your user directory `~/`.

### 3) SSH into VMs

There are two ways to SSH into your VM. You can either do it through command line or do it within VSCode which is the preferred method.

#### SSH with command line

1. Continue in your terminal, and run `vagrant ssh [VM Name]`, where `[VM Name]` is the name of the VM (ie. `ome1`, `gateway1`, `tp1`)
2. You will then be logged into the `vagrant` user, and your terminal should look something like `vagrant@vagrant:~/ $`
   1. If you for some reason need to do any changes on the repository (say running `cmake` or `make`), you will need superuser access because we ran these commands during the provisioning. If you need this type of access, just type in `sudo su`.

#### SSH with VSCode

0. Make sure to have the "Remote Explorer" VSCode extension, and navigate to the "Remote" targets in the dropdown menu.
1. Continue in your terminal, and run `vagrant ssh-config`
2. Copy the output with right click > copy
3. Click the Settings icon next to "SSH" > and choose your user's `[USER]/.ssh/config` file.
4. Paste the contents (except for any line with the prefix "`==> vagrant`") at the top of the file.
5. Click the Refresh icon next to "Remote" in VSCode's Remote Explorer
6. You should now see 3 new SSH targets appear, which should be named `ome1`, `gateway1`, and `tp1`.
7. Click on the new window icon next to whichever target you would like to open and it should open a new VSCode window and log in for you.
   1. If you are prompted to choose the OS, choose Linux.
   2. Furthermore, if you are doing any development work or looking through the repository, you should probably choose `Open Folder... > Choose ~/[REPOSITORY NAME] > Ok` to open the VSCode window exactly to the repository.

### 4) Running the program

The program should already be running. However, if you need to run any specific program please make sure to SSH into the correct VM and run the executable in the repository's `build/` directory with superuser permission through `sudo su`.

You should be able to connect to the VM and send a TCP message in OUCH format and receive a response depending on the type of message. Please look through the example main code in the repositories to get a handle how to exchange messages and receive them. Also you should check the IPs of the VMs and the port to make sure you are connected correctly for TCP.

As of 12/14/2022, please note the following VM settings:

- `ome1` - IPv4: 192.168.65.1, TCP server port 3000
- `gateway1` - IPv4: 192.168.64.1
- `tp1` - IPv4: 192.168.63.1

## Testing

To run test, you must follow the steps outlined in [Instructions for using the project](#instructions-for-using-the-project) and correctly SSH into the VM with the repository you want to run the test on, and you may need to change to the superuser by running `sudo su` in the terminal (you don't have to do this unless you get a permission denied error). The steps for testing are identical between the different repositories and is outlined below.

### Running the tests

1. SSH into the Machine (`vagrant ssh` or through VSCode)
2. Navigate to the repository's build folder (`~/[REPOSITORY NAME]/build`)
3. Run `./test`

If all tests are passing, you should see something akin to (following output from `ome1`):

```txt
===============================================================================
All tests passed (227 assertions in 20 test cases)
```

## Project Results

// TODO

## Screenshots and Example Outputs

See [EXAMPLE_OUTPUT.md](documentation/EXAMPLE_OUTPUT.md) // TODO

## Postmortem Summary

See [POST_MORTEM.md](documentation/POST_MORTEM.md) // TODO

## TODOs

See [MISSING_COMPONENTS.md](documentation/MISSING_COMPONENTS.md) // TODO
