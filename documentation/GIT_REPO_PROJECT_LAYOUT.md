# Sub Project File Layout

Last revision date: 12/14/2022

## Overall Project Layouts

Each project builds upon CMake and has the following simplified layout:

```txt
├── build
│   └── ... CMake files, Makefile, and Executables
├── CMakeLists.txt
├── Doxyfile
├── include
│   └── ... include file for project
├── init-vm.sh
├── lib
|   ├── CMakeLists.txt
│   └── ... outside libraries used and Git submodules
├── main
│   ├── main.cpp
│   └── ... other main files and examples
├── README.md
├── src
│   ├── CMakeLists.txt
│   └── ... source files
├── tests
│   ├── CMakeLists.txt
│   ├── tests.cpp
│   └── ... more tests
└── Vagrantfile
```

## Order Matching Engine

- include - This is empty for now
- lib - Stores all Git submodules
  - [SPSCQueue](https://github.com/rigtorp/SPSCQueue) - rigtorp/SPSCQueue, SPSC queue
  - [unordered_dense](https://github.com/martinus/unordered_dense) - martinus/unordered_dense, Robinhood hashmap for quicker hashing access
  - empty.cpp - Dummy .cpp file for compilation target for `lib` library
  - CMakeLists.txt - Compiles `lib` library from submodules used in project
- main - Examples and main executable src code. Ones in bold are used for running the project.
  - *byte_swap.cpp* - Example code for using `byte_swap`
  - *dll_main.cpp* - Example code for using `DoublyLinkedList` class
  - *main.cpp* - Main for testing code (as of 12/14/2022 contains simple tcp_client end for OME)
  - *multicast_receiver.cpp* - UDP Multicast receiver example
  - **ome_main.cpp - Main for OME**
  - *orderbook_main.cpp* - Example `OrderBook` usage
  - *spscqueue_main.cpp* - Example usage of rigtorp/SPSCQueue
  - *symbols.txt* - Text file of [top 10 symbols in NASDAQ 100 Index](https://www.slickcharts.com/nasdaq100) by weight
  - *tcp_client.cpp* - Example usage of boost::asio tcp client
  - *tcp_server.cpp* - Example usage of boost::asio tcp server
  - *timer_main.cpp* - Example use of `Timer` class
  - *unordered_dense_main.cpp* - Example use of martinus/unordered_dense
- src
  - CMakeLists.txt - Compiles `srclib` library with all sources
  - doublylinkedlist - `DoublyLinkedList` class use in `OrderBook`
    - *dll.h* - `DoublyLinkedList<T>`
    - *dll.hpp*
    - *dll_iterator.hpp* - Contains iterator for DLL
    - *util_ptr.h* - Contains util functions for `operator<` and destructor functions defined on pointers, values, and smart pointers
  - orderbook - `OrderBook` class
    - *orderbook.cpp*
    - *orderbook.h* - `OrderBook`
    - *orderbook_print.cpp* - `std::ostringstream` print tool for viewing `OrderBook` state
  - order_matching_engine - `OrderMatchingEngine, Timer` classes, `util` methods
    - *debug.h* - Used to set macros for debug printing.
    - *machine_time.cpp*
    - *machine_time.h* - `Timer`, `MachineTimer`, `SimpleTimer`
    - *order_matching_engine.cpp*
    - *order_matching_engine.h* - `OrderMatchingEngine`
    - *util.cpp*
    - *util.h* - Utility functions for adding two char arrays (w/ no `'\0'` at end), copying `std::string` to `char[]`, and more
  - parser - Contains code to parse to and from `unsigned char[]` to structs.
    - *parser.cpp*
    - *parser.h* - `Parser`, `OUCH_Parser`, `ITCH_Parser`
  - struct
    - *nasdaq_itch.h* - NASDAQ ITCH structs
    - *nasdaq_ouch.h* - NASDAQ OUCH inbound and outbound structs
    - *structs.h* - `Order`, `PriceLevel` structs for internal bookkeeping
  - user_interface
- tests
  - CMakeLists.txt
  - files
    - ...
  - *test_dll.cpp* - Testing for `DoublyLinkedList`. Only unit tests.
  - *test_orderbook.cpp* - Testing for `OrderBook`. Contains behavior-driven-testing and unit testing.
  - *test_order_matching_engine.cpp* - Testing for `OrderMatchingEngine` . Only behavior-driven-testing.
  - *test_parser.cpp* - Testing for `Parser`, `OUCH_Parser`, and `ITCH_Parser`. Only unit tests.
  - *tests.cpp* - Catch Main file
  - *test_struct_order.cpp* - Not used currently

## Gateway

## Tickerplant
