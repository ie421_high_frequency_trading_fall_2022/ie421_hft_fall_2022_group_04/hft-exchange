# Technologies

Here, I will provide a detailed descripton of all technologies, languages, frameworks, DevOps tools, etc. that I used along with a short rationale why they were used, other alternatives evaluated, and the decision to stick with this particular technology.

### Versions for tech stack used

On Host computer:

- Operating System
  - Windows 10 Education, Version 21H2
- Hardware Specifications
  - AMD Ryzen 5
  - 16GB RAM (recommended at least 8GB RAM total on computer, 4GB free)
  - 1TB Disk (recommended at least 64GB disk space)
- VirtualBox v6.1.38
- [Vagrant v2.3.0](https://developer.hashicorp.com/vagrant/downloads)
  - OME vagrant box: [`minimal/xenial64`](https://app.vagrantup.com/minimal/boxes/xenial64)
  - Gateway vagrant box: [`minimal/xenial64`](https://app.vagrantup.com/minimal/boxes/xenial64)
  - Tickerplant vagrant box: [`minimal/xenial64`](https://app.vagrantup.com/minimal/boxes/xenial64)

On VirtualMachine:

- G++/GCC (Ubuntu 11.3.0-1ubuntu1~22.04) 11.3.0
- Valgrind v3.18.1
- Doxygen v1.9.1
- Boost v1.74.0
- [Catch2 v2.13.x](https://github.com/catchorg/Catch2) - Unsure which exact version it is. This version is outdated as there is a new v3.x
- Git v2.34.1

GitHub Repositories:
- [ankerl/unordered_dense](https://github.com/martinus/unordered_dense) - `commit 39322aeb0ec2ca8c2722000066483d6c72c69136 (origin/main)`
- [rigtorp/SPSCQueue](rigtorp/SPSCQueue) - `commit 945c7c532bac85f028b386f2e2ab74d38e93e333 (origin/master)`
- [ToruNiina/toml11](https://github.com/ToruNiina/toml11) - `commit 22db720ad55c3470c4d036ae74be35e68c761845 (origin/master)`

## Language: C++20

We decided to go with C++ for a variety of reason. First, we wanted to both bolster our knowledge in C++ by working with C++11 tools like smart pointers and libraries like boost. Second, we also needed access and control to low level concepts to make sure we could fine tune the performance to match our exact specification while also having high-level tools available to Object Oriented Programming languages. Lastly, everyone in our group was familiar with the language and we built off our experience doing undergrad at UIUC where C++ is a core part of most engineering majors.

## Testing: Catch2

We decided to go with Catch2 solely because the group leader had experience with using it in a CS core class. It is also easy to learn and set up. We did not want to waste time learning a new testing suite and instead choose to spend the time toward implementing functionality. However, we may switch to [Google Test](https://github.com/google/googletest) in the future but we haven't had time to explore how different the functionality is between the two. Catch2 offers everything we need. The only problem with it currently is we are running on Catch2 v2 while Catch2 v3 is out (but we would have to compile it ourselves instead of using Ubuntu's package manager). Some of our testing code will need to be updated to reflect the change to version 3.

## Memory Checker: Valgrind

We did not explore any other memory leak checker as everyone is familiar with Valgrind and it does its job extremely well.

## Compiler and Debugger: GCC/G++, Gnu Debugger (GDB)

We used GCC/G++ because we are familiar with it and it is automatically added by Ubuntu's package manager. Another option would have been llvm but we did not want to deal with any complexities arising from different compilers and out-of-date packages. It should be a quick change in the CMakeLists.txt to incorporate llvm if one should want to do so.

## Documentation: Doxygen

We wanted a no-hassle and simple documentation framework. Doxygen was the perfect candidate because it offered both features and our group leader was also familiar with it from using it in a CS core class. Because of its ubiquitousness, it also means that there are plenty of articles, tutorials, and help should one find themselves stuck. Note that we also had to get GraphViz to help visualize the hierarchy of classes for Doxygen. It provides both Latex and HTML output which is very useful for visualizing.

## DevOps Tools: Vagrant, Git

### Vagrant

We choose Vagrant both because our professor was well-versed in it so he could help us if we encountered problems, as well as its ease of use. The only problem is you must get a VirtualBox license in order to use it, but you could go with VMWare as well (which also requires a license and which we did not test on). It was a slight learning curve to understand how to perform certain functionality, but once going, it was very simple to use.

### Git

Literally the #1 most used version control software. Copious amounts of tutorials, videos, articles, stack overflow posts, etc. We also used GitLab (as part of UIUC) so this is pretty much required.
