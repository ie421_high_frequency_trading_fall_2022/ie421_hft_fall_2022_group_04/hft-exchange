# Source Code Documentation

## Building Doxygen

Simple run the command `./$ doxygen Doxyfile` to build the Doxygen documentation.

The output will be located at `./doxygen/`. You can open the `index.html` file in this directory in your preferred browser to see the documentation.
