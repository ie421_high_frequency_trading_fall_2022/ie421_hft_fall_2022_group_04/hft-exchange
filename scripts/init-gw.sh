echo "Initialize Gateway"

sudo apt-get update
sudo apt-get upgrade -y

# Install C++
sudo apt-get install build-essential -y
gcc --version

# Install Boost
sudo apt-get install libboost-all-dev -y

# # Install Valgrind
# sudo apt-get -y install valgrind

# # Install GDB
# sudo apt-get -y install gdb

# Install CMake3
sudo apt-get install cmake -y
cmake --version

# Install Doxygen
# sudo apt-get -y install doxygen
# doxygen --version
## Install Graphviz
# sudo apt-get -y install graphviz

# Install Catch2
sudo apt-get -y install catch2
catch2 --version

# Git
sudo apt-get -y install git-all
git --version
# Set default editor to vim
git config --global core.editor "vim"
git config credential.helper store

# Clone repository
git -C /home/vagrant clone https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2022/ie421_hft_fall_2022_group_04/exchange-gateway.git
 
# Start the gateway
cd /home/vagrant/exchange-gateway
mkdir build
cd build
cmake ..
make -j4
# ./gateway_main ## TODO: Uncomment this when ready

echo "Gateawy finished"